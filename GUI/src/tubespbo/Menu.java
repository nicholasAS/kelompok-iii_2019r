/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubespbo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Ferani
 */
public class Menu extends Frame implements MouseListener {

    
    JPanel panel, panelbuttonregis, panelbuttonLeader, panelbuttonLogin, panelBackground, panelimg;
    Font font, font1;
    JLabel label, labelBg, labelimg;
    JButton buttonLogin, buttonRegis, buttonLeader;
    ImageIcon icon, iconimg;
    Timer tm;
    
    
    public Menu() {
        new playMusic("music/backsound.mp3");

        panel = new JPanel();
        panel.setBounds(10, 30, 480, 80);
        panel.setBackground(new Color(0, 255, 0, 0));

        font = new Font("Copperplate Gothic Bold", Font.BOLD, 40);
        label = new JLabel("Frogger The Game! ");
        label.setForeground(new Color(255, 255, 255));
        label.setFont(font);

        font1 = new Font("Berlin Sans FB Demi", Font.PLAIN, 16);

        //buttonregis
        panelbuttonregis = new JPanel();
        panelbuttonregis.setBounds(20, 360, 120, 60);
        panelbuttonregis.setBackground(new Color(0, 255, 0, 0));

        buttonRegis = new JButton("Register");
        buttonRegis.setBackground(Color.blue);
        buttonRegis.setForeground(Color.white);
        buttonRegis.setPreferredSize(new Dimension(120, 60));
        buttonRegis.setFont(font1);

        //buttonlogin
        panelbuttonLogin = new JPanel();
        panelbuttonLogin.setBounds(170, 360, 130, 60);
        panelbuttonLogin.setBackground(new Color(0, 255, 0, 0));

        buttonLogin = new JButton("Login");
        buttonLogin.setBackground(Color.red);
        buttonLogin.setForeground(Color.white);
        buttonLogin.setPreferredSize(new Dimension(130, 60));
        buttonLogin.setFont(font1);

        //buttonLeaderboard
        panelbuttonLeader = new JPanel();
        panelbuttonLeader.setBounds(320, 360, 130, 60);
        panelbuttonLeader.setBackground(new Color(0, 255, 0, 0));

        buttonLeader = new JButton("LeaderBoard");
        buttonLeader.setBackground(Color.orange);
        buttonLeader.setForeground(Color.white);
        buttonLeader.setPreferredSize(new Dimension(130, 60));
        buttonLeader.setFont(font1);

        //Panel Bg
        panelBackground = new JPanel();
        panelBackground.setBounds(0, 0, 500, 500);
        panelBackground.setBackground(new Color(0, 255, 0, 0));

        labelBg = new JLabel();
        icon = new ImageIcon(new ImageIcon("gambar/bgGreen1.jpg").getImage().getScaledInstance(600, 600, Image.SCALE_DEFAULT));
        labelBg.setIcon(icon);

        //PanelImage
        panelimg = new JPanel();
        panelimg.setBounds(100, 110, 280, 280);
        panelimg.setBackground(new Color(0, 255, 0, 0));

        labelimg = new JLabel();
        iconimg = new ImageIcon(new ImageIcon("gambar/frog.png").getImage().getScaledInstance(210, 210, Image.SCALE_DEFAULT));
        labelimg.setIcon(iconimg);

          //
        panel.add(label);

        panelimg.add(labelimg);
        panelbuttonregis.add(buttonRegis);
        panelbuttonregis.add(buttonRegis);
        panelbuttonLogin.add(buttonLogin);
        panelbuttonLeader.add(buttonLeader);
        panelBackground.add(labelBg);
          //

        frame.getContentPane().add(panelimg);
        frame.getContentPane().add(panelbuttonLeader);
        frame.getContentPane().add(panelbuttonLogin);
        frame.getContentPane().add(panelbuttonregis);
        frame.getContentPane().add(panel);
        frame.getContentPane().add(panelBackground);

        //Command for button
        buttonLogin.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tm.stop();
                new SoundEffect("music/button.wav");
                setUnVisible();
                new Login(frame,panelBackground);
            }
            
        });
        
        buttonRegis.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tm.stop();
                new SoundEffect("music/button.wav");
                setUnVisible();
                new Register(frame,panelBackground);

            }
        });

        buttonLeader.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tm.stop();
                new SoundEffect("music/button.wav");
                setUnVisible();
                new LeaderBoard(frame);

            }
        });
        
         //effect mouseover
        buttonLogin.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                buttonLogin.setBackground(Color.yellow);
                buttonLogin.setForeground(Color.red);
                new SoundEffect("music/boing.wav");
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                buttonLogin.setBackground(Color.red);
                buttonLogin.setForeground(Color.white);
            }
        });
        
        buttonRegis.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                buttonRegis.setBackground(Color.yellow);
                buttonRegis.setForeground(Color.red);
                new SoundEffect("music/boing.wav");
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                buttonRegis.setBackground(Color.BLUE);
                buttonRegis.setForeground(Color.white);
            }
        });
        
        buttonLeader.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                buttonLeader.setBackground(Color.yellow);
                buttonLeader.setForeground(Color.red);
                new SoundEffect("music/boing.wav");
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                buttonLeader.setBackground(Color.ORANGE);
                buttonLeader.setForeground(Color.white);
            }
        });
        
        //blinking text
        
        
        tm = new Timer(300,new ActionListener() {
            int a=0;
            @Override
            public void actionPerformed(ActionEvent e) {
                a++;
                if(a %2 == 0 ){
                    panel.setVisible(false);
                }else{
                    panel.setVisible(true);
                }
               
            }
            
           
        });
        tm.start();

        frame.setVisible(true);
    }

    public void setUnVisible() {
        panelbuttonregis.setVisible(false);
        panelbuttonLeader.setVisible(false);
        panelbuttonLogin.setVisible(false);
        panelBackground.setVisible(false);
        panelimg.setVisible(false);
        panel.setVisible(false);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
