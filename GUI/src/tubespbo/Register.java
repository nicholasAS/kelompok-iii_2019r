/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubespbo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Ferani
 */
public class Register{

    JPanel panelJudul, panelForm, panelSubmit, panelBack, panelLogin;
    JLabel labelJudul, username, password, country;
    Font font;
    JTextField fieldUser, fieldPass;
    JButton submit, back, login;
    JComboBox pilih;
    ImageIcon icon;

    public Register(final JFrame frame, JPanel bg) {

        //judul
        panelJudul = new JPanel();
        panelJudul.setBounds(0, 90, 500, 100);
        panelJudul.setBackground(new Color(255, 255, 255, 0));

        font = new Font("Copperplate Gothic Bold", Font.BOLD, 48);
        labelJudul = new JLabel("Register");
        labelJudul.setForeground(new Color(255, 255, 255));
        labelJudul.setFont(font);

        //PanelForm
        panelForm = new JPanel(new GridLayout(3, 1));
        panelForm.setBounds(40, 200, 350, 80);
        panelForm.setBackground(new Color(0, 255, 0, 10));

        //label
        username = new JLabel("Username ");
        username.setForeground(Color.yellow);
        fieldUser = new JTextField(20);

        password = new JLabel("Password");
        password.setForeground(Color.yellow);
        fieldPass = new JTextField(10);

        country = new JLabel("Country");
        country.setForeground(Color.yellow);

        Object arpilih[] = {"indonesia", "english"};
        pilih = new JComboBox(arpilih);

        panelSubmit = new JPanel();
        panelSubmit.setBounds(210, 290, 130, 40);
        panelSubmit.setBackground(new Color(255, 255, 255, 0));

        submit = new JButton("Register");
        submit.setBackground(Color.BLUE);
        submit.setForeground(Color.WHITE);
        submit.setPreferredSize(new Dimension(130, 40));

        //back Button
        panelBack = new JPanel();
        panelBack.setBounds(30, 350, 100, 70);
        panelBack.setBackground(new Color(255, 255, 255, 0));

        back = new JButton();
        icon = new ImageIcon(new ImageIcon("gambar/Back.png").getImage().getScaledInstance(70, 50, Image.SCALE_DEFAULT));
        back.setIcon(icon);
        back.setBackground(new Color(0, 255, 204));

        //Command For Button
        back.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new SoundEffect("music/button.wav");
                playMusic.player.close();
                frame.dispose();
                new Menu();
            }
        });

        //
        panelForm.add(username);
        panelForm.add(fieldUser);
        panelForm.add(password);
        panelForm.add(fieldPass);
        panelForm.add(country);
        panelForm.add(pilih);

        panelJudul.add(labelJudul);

        panelSubmit.add(submit);

        panelBack.add(back);

        frame.getContentPane().add(panelSubmit);
        frame.getContentPane().add(panelForm);
        frame.getContentPane().add(panelJudul);
        frame.getContentPane().add(panelBack);
        frame.getContentPane().add(bg);

        bg.setVisible(true);
    }


}
