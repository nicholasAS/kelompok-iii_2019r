/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubespbo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Ferani
 */
public class Lose extends Frame{
    JPanel panelLose ,imgPanel,panelButton;
    JLabel lose, imgLabel;
    Font font = new Font("Snap ITC", Font.BOLD, 50);
    Font font1 = new Font("Copperplate Gothic Bold", Font.BOLD, 15);
    ImageIcon loseIcon;
    JButton button;
    Timer t,t1;
    
    public Lose(){
        
        
        new SoundEffect("music/lose.wav");
        
        //lose
        panelLose = new JPanel();
        panelLose.setBounds(50,60,400,70);
        panelLose.setBackground(Color.black);
        
        lose = new JLabel("You Lose :(");
        lose.setFont(font);
        lose.setForeground(Color.red);
        panelLose.add(lose);
        
        //png
        loseIcon = new ImageIcon(new ImageIcon("gambar/lose.png").getImage().getScaledInstance(210,170, Image.SCALE_DEFAULT));
        imgLabel = new JLabel();
        imgLabel.setIcon(loseIcon);
        imgPanel = new JPanel();
        imgPanel.setBounds(10,140,220,200);
        imgPanel.setBackground(new Color(255,255,255,0));
        imgPanel.add(imgLabel);
        
        t1 = new Timer(50,new ActionListener() {
        int x = 10;
            @Override
            public void actionPerformed(ActionEvent e) {
              x = x +5;
             imgPanel.setBounds(x,140,220,200);
            }

           
        });
        t1.start();
        new SoundEffect("music/lose1.wav");
        
        //button
        button = new JButton("Try Again?");
        button.setBackground(Color.black);
        button.setForeground(Color.red);
        button.setPreferredSize(new Dimension(140, 60));
        button.setFont(font1);
        
        panelButton = new JPanel();
        panelButton.setBounds(160,320,150,70);
        panelButton.setBackground(new Color(255,255,255,0));
        panelButton.add(button);
        panelButton.setVisible(false);
        
        
        
        t = new Timer(3000,new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              panelButton.setVisible(true);
                
            }

           
        });
        t.start();
        
        
        frame.getContentPane().add(panelButton);
        frame.getContentPane().add(imgPanel);
        frame.getContentPane().setBackground(Color.GRAY);
        frame.getContentPane().add(panelLose);
        frame.setVisible(true);
        
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                button.setBackground(Color.red);
                button.setForeground(Color.black);
                
                
                new SoundEffect("music/boing.wav");
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                button.setBackground(Color.black);
                button.setForeground(Color.red);
            }
        });
    }
}
